module.exports = {
  "pages": "src/pages",
  "sourceDir": "public",
  "routifyDir": ".routify",
  "ignore": "",
  "dynamicImports": true,
  "singleBuild": false,
  "noHashScroll": false,
  "distDir": "dist",
  "hashScroll": true,
  "extensions": [
    "html",
    "svelte",
    "md",
    "svx"
  ],
  "started": "2021-10-27T10:04:01.094Z"
}