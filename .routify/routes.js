
/**
 * @roxi/routify 2.18.3
 * File generated Wed Oct 27 2021 12:39:13 GMT+0200 (Central European Summer Time)
 */

export const __version = "2.18.3"
export const __timestamp = "2021-10-27T10:39:13.015Z"

//buildRoutes
import { buildClientTree } from "@roxi/routify/runtime/buildRoutes"

//imports


//options
export const options = {}

//tree
export const _tree = {
  "name": "_layout",
  "filepath": "/_layout.svelte",
  "root": true,
  "ownMeta": {
    "preload": "proximity"
  },
  "absolutePath": "/Users/sewek/Projects/examples/tailwind-example/src/pages/_layout.svelte",
  "children": [
    {
      "isFile": true,
      "isDir": false,
      "file": "_fallback.svelte",
      "filepath": "/_fallback.svelte",
      "name": "_fallback",
      "ext": "svelte",
      "badExt": false,
      "absolutePath": "/Users/sewek/Projects/examples/tailwind-example/src/pages/_fallback.svelte",
      "importPath": "../src/pages/_fallback.svelte",
      "isLayout": false,
      "isReset": false,
      "isIndex": false,
      "isFallback": true,
      "isPage": false,
      "ownMeta": {},
      "meta": {
        "recursive": true,
        "preload": "proximity",
        "prerender": true
      },
      "path": "/_fallback",
      "id": "__fallback",
      "component": () => import('../src/pages/_fallback.svelte').then(m => m.default)
    },
    {
      "isFile": false,
      "isDir": true,
      "file": "aboutme",
      "filepath": "/aboutme",
      "name": "aboutme",
      "ext": "",
      "badExt": false,
      "absolutePath": "/Users/sewek/Projects/examples/tailwind-example/src/pages/aboutme",
      "children": [
        {
          "isFile": true,
          "isDir": false,
          "file": "index.svelte",
          "filepath": "/aboutme/index.svelte",
          "name": "index",
          "ext": "svelte",
          "badExt": false,
          "absolutePath": "/Users/sewek/Projects/examples/tailwind-example/src/pages/aboutme/index.svelte",
          "importPath": "../src/pages/aboutme/index.svelte",
          "isLayout": false,
          "isReset": false,
          "isIndex": true,
          "isFallback": false,
          "isPage": true,
          "ownMeta": {},
          "meta": {
            "recursive": true,
            "preload": "proximity",
            "prerender": true
          },
          "path": "/aboutme/index",
          "id": "_aboutme_index",
          "component": () => import('../src/pages/aboutme/index.svelte').then(m => m.default)
        },
        {
          "isFile": true,
          "isDir": false,
          "file": "README.md",
          "filepath": "/aboutme/README.md",
          "name": "README",
          "ext": "md",
          "badExt": false,
          "absolutePath": "/Users/sewek/Projects/examples/tailwind-example/src/pages/aboutme/README.md",
          "importPath": "../src/pages/aboutme/README.md",
          "isLayout": false,
          "isReset": false,
          "isIndex": false,
          "isFallback": false,
          "isPage": true,
          "ownMeta": {},
          "meta": {
            "recursive": true,
            "preload": "proximity",
            "prerender": true
          },
          "path": "/aboutme/README",
          "id": "_aboutme_README",
          "component": () => import('../src/pages/aboutme/README.md').then(m => m.default)
        }
      ],
      "isLayout": false,
      "isReset": false,
      "isIndex": false,
      "isFallback": false,
      "isPage": false,
      "ownMeta": {},
      "meta": {
        "recursive": true,
        "preload": "proximity",
        "prerender": true
      },
      "path": "/aboutme"
    },
    {
      "isFile": true,
      "isDir": false,
      "file": "index.svelte",
      "filepath": "/index.svelte",
      "name": "index",
      "ext": "svelte",
      "badExt": false,
      "absolutePath": "/Users/sewek/Projects/examples/tailwind-example/src/pages/index.svelte",
      "importPath": "../src/pages/index.svelte",
      "isLayout": false,
      "isReset": false,
      "isIndex": true,
      "isFallback": false,
      "isPage": true,
      "ownMeta": {},
      "meta": {
        "recursive": true,
        "preload": "proximity",
        "prerender": true
      },
      "path": "/index",
      "id": "_index",
      "component": () => import('../src/pages/index.svelte').then(m => m.default)
    },
    {
      "isFile": true,
      "isDir": true,
      "file": "_layout.svelte",
      "filepath": "/posts/_layout.svelte",
      "name": "_layout",
      "ext": "svelte",
      "badExt": false,
      "absolutePath": "/Users/sewek/Projects/examples/tailwind-example/src/pages/posts/_layout.svelte",
      "children": [
        {
          "isFile": false,
          "isDir": true,
          "file": "1",
          "filepath": "/posts/1",
          "name": "1",
          "ext": "",
          "badExt": false,
          "absolutePath": "/Users/sewek/Projects/examples/tailwind-example/src/pages/posts/1",
          "children": [
            {
              "isFile": true,
              "isDir": false,
              "file": "index.svelte",
              "filepath": "/posts/1/index.svelte",
              "name": "index",
              "ext": "svelte",
              "badExt": false,
              "absolutePath": "/Users/sewek/Projects/examples/tailwind-example/src/pages/posts/1/index.svelte",
              "importPath": "../src/pages/posts/1/index.svelte",
              "isLayout": false,
              "isReset": false,
              "isIndex": true,
              "isFallback": false,
              "isPage": true,
              "ownMeta": {},
              "meta": {
                "recursive": true,
                "preload": "proximity",
                "prerender": true
              },
              "path": "/posts/1/index",
              "id": "_posts_1_index",
              "component": () => import('../src/pages/posts/1/index.svelte').then(m => m.default)
            }
          ],
          "isLayout": false,
          "isReset": false,
          "isIndex": false,
          "isFallback": false,
          "isPage": false,
          "ownMeta": {},
          "meta": {
            "recursive": true,
            "preload": "proximity",
            "prerender": true
          },
          "path": "/posts/1"
        },
        {
          "isFile": false,
          "isDir": true,
          "file": "2",
          "filepath": "/posts/2",
          "name": "2",
          "ext": "",
          "badExt": false,
          "absolutePath": "/Users/sewek/Projects/examples/tailwind-example/src/pages/posts/2",
          "children": [
            {
              "isFile": true,
              "isDir": false,
              "file": "index.svelte",
              "filepath": "/posts/2/index.svelte",
              "name": "index",
              "ext": "svelte",
              "badExt": false,
              "absolutePath": "/Users/sewek/Projects/examples/tailwind-example/src/pages/posts/2/index.svelte",
              "importPath": "../src/pages/posts/2/index.svelte",
              "isLayout": false,
              "isReset": false,
              "isIndex": true,
              "isFallback": false,
              "isPage": true,
              "ownMeta": {},
              "meta": {
                "recursive": true,
                "preload": "proximity",
                "prerender": true
              },
              "path": "/posts/2/index",
              "id": "_posts_2_index",
              "component": () => import('../src/pages/posts/2/index.svelte').then(m => m.default)
            }
          ],
          "isLayout": false,
          "isReset": false,
          "isIndex": false,
          "isFallback": false,
          "isPage": false,
          "ownMeta": {},
          "meta": {
            "recursive": true,
            "preload": "proximity",
            "prerender": true
          },
          "path": "/posts/2"
        },
        {
          "isFile": true,
          "isDir": false,
          "file": "index.svelte",
          "filepath": "/posts/index.svelte",
          "name": "index",
          "ext": "svelte",
          "badExt": false,
          "absolutePath": "/Users/sewek/Projects/examples/tailwind-example/src/pages/posts/index.svelte",
          "importPath": "../src/pages/posts/index.svelte",
          "isLayout": false,
          "isReset": false,
          "isIndex": true,
          "isFallback": false,
          "isPage": true,
          "ownMeta": {},
          "meta": {
            "recursive": true,
            "preload": "proximity",
            "prerender": true
          },
          "path": "/posts/index",
          "id": "_posts_index",
          "component": () => import('../src/pages/posts/index.svelte').then(m => m.default)
        },
        {
          "isFile": true,
          "isDir": false,
          "file": "README.md",
          "filepath": "/posts/README.md",
          "name": "README",
          "ext": "md",
          "badExt": false,
          "absolutePath": "/Users/sewek/Projects/examples/tailwind-example/src/pages/posts/README.md",
          "importPath": "../src/pages/posts/README.md",
          "isLayout": false,
          "isReset": false,
          "isIndex": false,
          "isFallback": false,
          "isPage": true,
          "ownMeta": {},
          "meta": {
            "recursive": true,
            "preload": "proximity",
            "prerender": true
          },
          "path": "/posts/README",
          "id": "_posts_README",
          "component": () => import('../src/pages/posts/README.md').then(m => m.default)
        }
      ],
      "isLayout": true,
      "isReset": false,
      "isIndex": false,
      "isFallback": false,
      "isPage": false,
      "importPath": "../src/pages/posts/_layout.svelte",
      "ownMeta": {},
      "meta": {
        "recursive": true,
        "preload": "proximity",
        "prerender": true
      },
      "path": "/posts",
      "id": "_posts__layout",
      "component": () => import('../src/pages/posts/_layout.svelte').then(m => m.default)
    },
    {
      "isFile": true,
      "isDir": false,
      "file": "README.md",
      "filepath": "/README.md",
      "name": "README",
      "ext": "md",
      "badExt": false,
      "absolutePath": "/Users/sewek/Projects/examples/tailwind-example/src/pages/README.md",
      "importPath": "../src/pages/README.md",
      "isLayout": false,
      "isReset": false,
      "isIndex": false,
      "isFallback": false,
      "isPage": true,
      "ownMeta": {},
      "meta": {
        "recursive": true,
        "preload": "proximity",
        "prerender": true
      },
      "path": "/README",
      "id": "_README",
      "component": () => import('../src/pages/README.md').then(m => m.default)
    }
  ],
  "isLayout": true,
  "isReset": false,
  "isIndex": false,
  "isFallback": false,
  "isPage": false,
  "isFile": true,
  "file": "_layout.svelte",
  "ext": "svelte",
  "badExt": false,
  "importPath": "../src/pages/_layout.svelte",
  "meta": {
    "preload": "proximity",
    "recursive": true,
    "prerender": true
  },
  "path": "/",
  "id": "__layout",
  "component": () => import('../src/pages/_layout.svelte').then(m => m.default)
}


export const {tree, routes} = buildClientTree(_tree)

