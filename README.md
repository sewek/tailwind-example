# Praca z ćwiczeniem

#### Pobieranie

1. Pobieramy git'a i node'a, jeśli ich jeszcze nie mamy. [GIT](https://git-scm.com/downloads), [NodeJs](https://nodejs.org/en/download/)
2. Wchodzimy w katalog w którym chcemy pracować, otwieramy w nim wiersz poleceń(shift+ppm -> otwórz CMD tutaj); następnie wprowadzamy: `git clone https://gitlab.com/sewek/tailwind-example.git`
3. Po sklonowaniu przechodzimy do folderu: `cd tailwind-example`
4. I zaznaczamy główną gałąź i tworzymy swoją: `git checkout main && git checkout -b <imie>`

Ewentualnie wchodzimy tutaj [https://gitlab.com/sewek/tailwind-example]() i po prostu klikamy Clone -> Visual Studio Code(HTTPS) i używamy interfejsu git'a w VS Code.

#### Praca

Jeśli jeszcze nie uruchamialiśmy aplikacji to uruchamiamy komendę `npm install` która instaluje wszystkie potrzebne pakiety.

# Uruchamianie

Run with `npm run <command>`, for example `npm run dev`

| Command     | Description                                   |
| ----------- | --------------------------------------------- |
| `dev`     | Development mode (port 5000)                  |
| `build`   | Build your app for production!                |
| `preview` | Preview the built version of your app locally |
