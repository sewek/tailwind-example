## Foldery/pliki i ich opisy

#### Folder /pages

Folder /pages zawiera wszystkie pliki i foldery, które będą dostepne z poziomu przeglądarki.

Odpowiednio w każdym katalogu mamy pliki:

* _fallback.svelte - plik który wyświetla się podczas gdy nie ma takiej strony(błąd 404)
* _layout.svelte - plik który jest layoutem, czyli wszystkie pliki w obrębie folderu w którym jest plik _layout będą miały nakładkę z tego pliku; w pliku _layout możemy wrzucać nagłówek(header) i stopkę(footer) i będą dostępne później w podstronach
* index.svelte - domyślny plik, który wyświetla się jak odtworzymy jakąś podstronę; przykładowo jak mamy w przeglądarce adres "/posts", to zostanie wyświetlony plik /posts/index.svelte
* inne pliki(z rozszerzeniem .svelte) i foldery bez żadnych podkreśleń będą normalnie dostępne w przeglądarce
* pliki inne z innymi rozszerzeniami i podkreśleniem(_) nie będą widoczne

#### Plik App.svelte i main.js

Pliki zawierające niezbędny kod do odpalenia aplikacji. Nie wymaga edycji.
