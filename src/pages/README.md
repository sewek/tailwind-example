## Strona główna i layout

#### Strona główna

Na stronie głównej(index.svelte) ma znaleźć się nagłówek oraz krótki opis strony pod nim(może być lorem ipsum: [https://www.lipsum.com/]()).

#### Layout

Layout powinien zawierać menu poziome, z ciemnym paskiem jako tło rozciągającym się na całą szerokość. Reszta zawartości strony powinna mieć stałą szerokość zależnie od urządzenia na którym wyświetlamy. Przykładowo na telefonach jest to 100%, ale już na desktopach można zmniejszyć, przykład: `2xl:w-2/3 xl:w-3/4` czyli dla szerokości 1536px i więcej będzie to 66,(6)%, a dla 1280px i więcej będzie to 75%. Dodatkowo możemy go wyrównać w poziomie: `mx-auto`
