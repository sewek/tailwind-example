## Strona "O mnie"

Stworzyć prostą stronę o mnie z jednym zdjęciem profilowym i treścią na kilka linii(może być lorem ipsum: [https://www.lipsum.com/]()).

Koncepcje do wyboru:

1. Zdjęcie profilowe na górze, wyśrodkowane w poziomie, tekst poniżej wyjustowany
2. Zdjęcie profilowe po prawej/lewej stronie i po drugiej tekst też wyjustowany
3. Dowolna inna wyglądająca ciekawie

Poniżej wszystkiego prosty formularz z nagłówkiem "Kontakt" zawierający pola tekstowe z labelami:

1. Email
2. Tytuł
3. Treść

Pod polami tekstowymi przycisk "Wyślij"
