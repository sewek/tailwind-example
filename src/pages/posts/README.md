## Posty

#### Lista postów

Lista postów ma zawierać posty w postaci bloczków z tytułem, niżej treścią ograniczoną do czterech zdań i po prawej na dole drobnymi literami autora, który jest linkiem do about me i datę. Po kliknięciu w taki bloczek przenosi nas do strony postu; na przykład: /posts/1 wyświetli plik /posts/1/index.svelte

#### Post

Post ma mieć obrazek na górze, później nagłówek i dalej treść z jakimś fajnym podziałem na paragrafy `<p></p>`. Poniżej autor i data.

Tutaj fajna ściągawka artykułów: [https://www.newscientist.com/subject/technology/]()

Takie pliki jak obrazki musisz wrzucić do folderu /public/posts
